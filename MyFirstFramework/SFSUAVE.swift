//
//  SUAVE.swift
//  SUAVE
//
//  Created by Qi Hang Chong on 05/01/22.
//
import Argon2Swift
import SwiftOTP
import Foundation

public struct SFSuaveRequest {
    public init() {}
    public var passwordRequest: SFPasswordRequest?
    public var authTokenRequest: SFAuthTokenRequest?
    public var authType: SFAuthType?
}


public enum SFGetAuthTokenType: String {
    case manual
    case automatic
}

public enum SFAuthType: String {
    case password
    case authToken
    case both
}

public struct SFResult {
    public init() {}
    public var isSuccess: Bool?
    public var authType: SFAuthType?
    public var responseReceivedTime: String?
    public var errorResponse: String?
}

public struct SFPasswordRequest {
    public init() {}
    public var userName: String?
    public var password: String?
    public var saltValue: String?
    public var encryptionKey : String?
    public var encryptedPassword: String?
}

public struct SFAuthTokenRequest {
    public init() {}
    public var secretKey: String?
    public var userEnteredAuthToken: String?
    public var authTokenType: SFGetAuthTokenType?
    public var digits: Int?
    public var timeInterval: Int?
    public var algorithm: OTPAlgorithm?
}

public struct SFAuthTokenResult {
    public init() {}
    public var isValid: Bool?
    public var requestedToken: String?
    public var error: String?
}


public struct SFAuthTokenAPIResponse {
    public init() {}
    public var responseCode: Int?
    public var message: String?
    public var authToken: String?
}

extension SFAuthTokenAPIResponse: Decodable {
    enum ErrorCodingKeys: String, CodingKey {
        case responseCode
        case message
        case authToken
    }
    public init(from decoder: Decoder) throws {
        let errorContainer = try decoder.container(keyedBy: ErrorCodingKeys.self)
        responseCode = try errorContainer.decodeIfPresent(Int.self, forKey: .responseCode) ?? 0
        message = try errorContainer.decodeIfPresent(String.self, forKey: .message) ?? "There is an error in the service"
        authToken = try errorContainer.decodeIfPresent(String.self, forKey: .authToken) ?? ""
    }
}


public class SFSUAVE {
    public init() {}
    public func validateEngineer(_ suaveRequest: SFSuaveRequest, completion: @escaping (SFAuthTokenResult) -> ()) {
        
        if suaveRequest.authType == .password {
            guard let passwordRequest = suaveRequest.passwordRequest else {
                var result = SFAuthTokenResult()
                result.error = "Invalid Password Request"
                completion(result)
                return
            }
            completion(self.passwordValidation(passwordRequest))
        } else if suaveRequest.authType == .authToken {
            guard let authRequest = suaveRequest.authTokenRequest else {
                var result = SFAuthTokenResult()
                result.error = "Invalid AuthToken Request"
                completion(result)
                return
            }
            if authRequest.authTokenType == .manual {
                completion(self.validateManual(authRequest))
            } else if authRequest.authTokenType == .automatic {
                var result = SFAuthTokenResult()
                let validateInputFields = validateInputFields(authRequest)
                
                if validateInputFields.error?.count ?? 0 > 0 {
                    completion(validateInputFields)
                    return
                }
                guard let data = base32DecodeToData(authRequest.secretKey ?? "") else {
                    result.isValid = false
                    result.error = "Invalid secretKey data"
                    result.requestedToken = ""
                    completion(result)
                    return
                }
                self.handleAutomaticAuthValidation(data, authRequest, completion)
            }
        } else if suaveRequest.authType == .both {
            guard let passwordRequest = suaveRequest.passwordRequest else {
                var result = SFAuthTokenResult()
                result.error = "Invalid Password Request"
                completion(result)
                return
            }
            let result = self.passwordValidation(passwordRequest)
            if result.isValid ?? false {
                guard let authRequest = suaveRequest.authTokenRequest else {
                    var result = SFAuthTokenResult()
                    result.error = "Invalid AuthToken Request"
                    completion(result)
                    return
                }
                if authRequest.authTokenType == .manual {
                    completion(self.validateManual(authRequest))
                } else if authRequest.authTokenType == .automatic {
                    var result = SFAuthTokenResult()
                    let validateInputFields = validateInputFields(authRequest)
                    
                    if validateInputFields.error?.count ?? 0 > 0 {
                        completion(validateInputFields)
                        return
                    }
                    guard let data = base32DecodeToData(authRequest.secretKey ?? "") else {
                        result.isValid = false
                        result.error = "Invalid secretKey data"
                        result.requestedToken = ""
                        completion(result)
                        return
                    }
                    self.handleAutomaticAuthValidation(data, authRequest, completion)
                }
            } else {
                completion(result)
            }
        }
    }
}

// Password validation
extension SFSUAVE {
    
    fileprivate func passwordValidation(_ passwordRequest: SFPasswordRequest) -> SFAuthTokenResult {
        var result = SFAuthTokenResult()
        result.isValid = false
        result.error = "Invalid Password Request Object"
        result = validatePasswordFields(passwordRequest)
        if result.error?.count ?? 0 > 0 {
            return result
        }
        if self.validatePassword(passwordRequest.userName ?? "", password: passwordRequest.password ?? "" , salt: passwordRequest.saltValue ?? "", encryptedPassword: passwordRequest.encryptedPassword ?? "") {
            result.isValid = true
            result.error = ""
            return result
        } else {
            result.isValid = false
            result.error = "Password validation failed"
        }
        return result
    }
    
    private func validatePasswordFields(_ passwordRequest: SFPasswordRequest) -> SFAuthTokenResult {
        var error = SFAuthTokenResult()
        guard passwordRequest.userName != nil, passwordRequest.userName?.count ?? 0 != 0 else {
            error.error = "Empty Username"
            return error
        }
        guard passwordRequest.password != nil, passwordRequest.password?.count ?? 0 != 0 else {
            error.error = "Empty Password"
            return error
        }
        guard passwordRequest.saltValue != nil, passwordRequest.saltValue?.count ?? 0 != 0 else {
            error.error = "Empty Salt"
            return error
        }
        guard passwordRequest.saltValue != nil, passwordRequest.encryptedPassword?.count ?? 0 != 0 else {
            error.error = "Empty encryptedPassword"
            return error
        }
        return error
    }
    
    private func validatePassword(_ userName: String, password: String, salt: String, encryptedPassword: String) -> Bool {
        
        /*
        func randomString(length: Int) -> String {
          let letters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
          return String((0..<length).map{ _ in letters.randomElement()! })
        }*/
        let saltValue = salt
        print(saltValue)
        let saltData = Data(saltValue.utf8)

        let salt = Salt(bytes: saltData)
        
        //Hash with pre-set params (iterations: 32, memory: 256, parallelism: 2, length: 32, type: Argon2Type.i, version: Argon2Version.V13)
        let result = try! Argon2Swift.hashPasswordString(password: password, salt: salt)
        
        print(result)
        //Raw hash values available as int list, base 64 string, and hex string
        /*
        let hashData = result.hashData()
        print(hashData)
        let base64Hash = result.base64String()
        print(base64Hash)
        let hexHash = result.hexString()
        print(hexHash)

        //Encoded hash values available as int list and encoded string
        let encodedData = result.encodedData()
        print(encodedData)*/
        let encodedString = result.encodedString()
        print(encodedString)

        //Verify password (returns true/false), uses default type (Argon2Type.i)
        let verified = try! Argon2Swift.verifyHashString(password: password, hash: encodedString)
        print(verified)
        
        if encryptedPassword == encodedString {
            return true
        }
        // we have to encrypt the value and compare to know its valid or not.

        return verified
    }

}


// Auth Code validation - Manual
extension SFSUAVE {
    
    private func validateInputFields(_ authRequest: SFAuthTokenRequest) -> SFAuthTokenResult {
        var result = SFAuthTokenResult()
        result.error = ""
        guard authRequest.secretKey != nil, authRequest.secretKey?.count ?? 0 != 0 else {
            result.isValid = false
            result.error = "SecretKey is empty, Please request with valid key"
            result.requestedToken = ""
            return result
        }
        guard authRequest.digits != nil, authRequest.digits ?? 0 != 0 else {
            result.isValid = false
            result.error = "AuthToken digit is empty, Please request with valid digit"
            result.requestedToken = ""
            return result
        }
        
        guard authRequest.timeInterval != nil, authRequest.timeInterval ?? 0 != 0 else {
            result.isValid = false
            result.error = "TimeInterval is empty, Please request with valid timeInterval"
            result.requestedToken = ""
            return result
        }
        
        guard authRequest.algorithm != nil else {
            result.isValid = false
            result.error = "Algorithm is empty, Please request with valid algorithm"
            result.requestedToken = ""
            return result
        }
        return result
    }
    
    func validateManual(_ authRequest: SFAuthTokenRequest) -> SFAuthTokenResult {
        var result = SFAuthTokenResult()
        if authRequest.userEnteredAuthToken?.count ?? 0 == 0 {
            result.isValid = false
            result.error = "Invalid AuthToken"
            result.requestedToken = authRequest.userEnteredAuthToken
            return result
        }
        let validateInputFields = validateInputFields(authRequest)
        
        guard let _ = validateInputFields.error else {
            return validateInputFields
        }
        guard let data = base32DecodeToData(authRequest.secretKey ?? "") else {
            result.isValid = false
            result.error = "Invalid data"
            result.requestedToken = authRequest.userEnteredAuthToken
            return result
        }
        let totp = TOTP(secret: data, digits: authRequest.digits ?? 6, timeInterval: authRequest.timeInterval ?? 120, algorithm: authRequest.algorithm ?? .sha1)
        let localOTP = totp?.generate(time: Date())
        
        if localOTP?.count ?? 0 == 0 {
            result.isValid = false
            result.error = "Local token is empty, Please contact framework team"
            result.requestedToken = authRequest.userEnteredAuthToken
            return result
        }
        if localOTP == authRequest.userEnteredAuthToken {
            result.isValid = true
            result.error = ""
            result.requestedToken = authRequest.userEnteredAuthToken
            return result
        }
        result.isValid = false
        result.error = "Something went wrong, Please contact framework team"
        result.requestedToken = authRequest.userEnteredAuthToken
        return result
    }
    
    public func getAuthToken(_ authRequest: SFAuthTokenRequest) -> String {
        var result = SFAuthTokenResult()
        if authRequest.userEnteredAuthToken?.count ?? 0 == 0 {
            result.isValid = false
            result.error = "Invalid AuthToken"
            result.requestedToken = authRequest.userEnteredAuthToken
            return "error"
        }
        let validateInputFields = validateInputFields(authRequest)
        
        guard let _ = validateInputFields.error else {
            return "error"
        }
        guard let data = base32DecodeToData(authRequest.secretKey ?? "") else {
            result.isValid = false
            result.error = "Invalid data"
            result.requestedToken = authRequest.userEnteredAuthToken
            return "error"
        }
        let totp = TOTP(secret: data, digits: authRequest.digits ?? 6, timeInterval: authRequest.timeInterval ?? 120, algorithm: authRequest.algorithm ?? .sha1)
        let localOTP = totp?.generate(time: Date())
        return localOTP ?? "empty"
    }

}


// Auth Code validation - Automatic

extension SFSUAVE {
    func getAuthToken(completionHandler: @escaping (SFAuthTokenAPIResponse?) -> Void) {
        let url = URL(string: "domainUrlString" + "films/")!

        let sessionConfig = URLSessionConfiguration.default
        sessionConfig.timeoutIntervalForRequest = 300.0
        sessionConfig.timeoutIntervalForResource = 300.0
        let session = URLSession(configuration: sessionConfig)
        let task = session.dataTask(with: url, completionHandler: { (data, response, error) in
          if let error = error {
            print("Error with fetching films: \(error)")
              completionHandler(nil)
              return
          }
          
          guard let httpResponse = response as? HTTPURLResponse, (200...299).contains(httpResponse.statusCode) else {
            print("Error with the response, unexpected status code: \(String(describing: response))")
            completionHandler(nil)
            return
          }

          if let data = data,
            let response = try? JSONDecoder().decode(SFAuthTokenAPIResponse.self, from: data) {
              completionHandler(response)
          } else {
              completionHandler(nil)
          }
        })
        task.resume()
    }
    
    fileprivate func handleAutomaticAuthValidation(_ data: Data, _ authRequest: SFAuthTokenRequest, _ completion: @escaping (SFAuthTokenResult) -> ()) {
        var result = SFAuthTokenResult()
        self.getAuthToken { (response) in
            if let res = response, res.authToken?.count ?? 0 > 0 {
                let totp = TOTP(secret: data, digits: authRequest.digits ?? 6, timeInterval: authRequest.timeInterval ?? 120, algorithm: authRequest.algorithm ?? .sha1)
                let localOTP = totp?.generate(time: Date())
                if res.authToken == localOTP {
                    result.isValid = true
                    result.error = ""
                    result.requestedToken = res.authToken
                    completion(result)
                } else {
                    result.isValid = false
                    result.error = "Not matching with server auth token"
                    result.requestedToken = res.authToken
                    completion(result)
                }
            } else {
                result.isValid = false
                result.error = "unexpected api response error"
                result.requestedToken = ""
                completion(result)
            }
        }
    }
}
